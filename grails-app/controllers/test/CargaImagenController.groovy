package test

import org.springframework.dao.DataIntegrityViolationException

class CargaImagenController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [cargaImagenInstanceList: CargaImagen.list(params), cargaImagenInstanceTotal: CargaImagen.count()]
    }

    def create() {
        [cargaImagenInstance: new CargaImagen(params)]
    }

    def save() {
        //Define la carpeta en donde se va almacenar el archivo
        String path =   ("${System.getProperty('user.home')}/imagenes/prueba")
        //Crear folder contenedor si no existe
        File folder = new File(path)
        if (!folder.exists()) {
            folder.mkdirs()
        }
        //Valida que el archivo para certificado digital no esté vacío y el tamaño del archivo sea menor a 100 KB
        if (!params.imagen?.empty && params.imagen.size < 1024 * 100) {
            //Obtiene el nombre original del archivo
            String nombreArchivo    =   params.imagen.getOriginalFilename()
            //Transfiere el archivo al destino indicado
            params.imagen.transferTo(new File(path + "/" + nombreArchivo))
            //Guarda en los parametros la ruta del archivo
            params.urlImagen    =   (path + "/" + nombreArchivo)
        }
        def cargaImagenInstance = new CargaImagen(params)
        if (!cargaImagenInstance.save(flush: true)) {
            render(view: "create", model: [cargaImagenInstance: cargaImagenInstance])
            return
        }
        

        flash.message = message(code: 'default.created.message', args: [message(code: 'cargaImagen.label', default: 'CargaImagen'), cargaImagenInstance.id])
        redirect(action: "show", id: cargaImagenInstance.id)
    }

    def show(Long id) {
        def cargaImagenInstance = CargaImagen.get(id)
        if (!cargaImagenInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'cargaImagen.label', default: 'CargaImagen'), id])
            redirect(action: "list")
            return
        }

        [cargaImagenInstance: cargaImagenInstance]
    }

    def edit(Long id) {
        def cargaImagenInstance = CargaImagen.get(id)
        if (!cargaImagenInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'cargaImagen.label', default: 'CargaImagen'), id])
            redirect(action: "list")
            return
        }

        [cargaImagenInstance: cargaImagenInstance]
    }

    def update(Long id, Long version) {
        def cargaImagenInstance = CargaImagen.get(id)
        if (!cargaImagenInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'cargaImagen.label', default: 'CargaImagen'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (cargaImagenInstance.version > version) {
                cargaImagenInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'cargaImagen.label', default: 'CargaImagen')] as Object[],
                          "Another user has updated this CargaImagen while you were editing")
                render(view: "edit", model: [cargaImagenInstance: cargaImagenInstance])
                return
            }
        }

        cargaImagenInstance.properties = params

        if (!cargaImagenInstance.save(flush: true)) {
            render(view: "edit", model: [cargaImagenInstance: cargaImagenInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'cargaImagen.label', default: 'CargaImagen'), cargaImagenInstance.id])
        redirect(action: "show", id: cargaImagenInstance.id)
    }

    def delete(Long id) {
        def cargaImagenInstance = CargaImagen.get(id)
        if (!cargaImagenInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'cargaImagen.label', default: 'CargaImagen'), id])
            redirect(action: "list")
            return
        }

        try {
            cargaImagenInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'cargaImagen.label', default: 'CargaImagen'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'cargaImagen.label', default: 'CargaImagen'), id])
            redirect(action: "show", id: id)
        }
    }

    def renderizarImagen(long id) {
        //Busca objeto CargaImagen con el id mandado desde la vista
        def cargaImagenInstance = CargaImagen.get(id)
        //Verifica que el objeto encontrado tenga la imagen
        if (cargaImagenInstance?.imagen) {
            //Envía como respuesta el tamaño de la imagen
            response.setContentLength(cargaImagenInstance.imagen.size())
            response.outputStream.write(cargaImagenInstance.imagen)            
        } else {
            response.sendError(404)
        }
    }
}