package test

class Catalogo2Controller {

	static scaffold = true

    def cambiaComboAlumno(Long idMateria) {
    	Materia materiaInstance = Materia.get(idMateria)
    	render(template: 'seleccionAlumno', model:  [alumnos: materiaInstance.alumnos])
    }
}
