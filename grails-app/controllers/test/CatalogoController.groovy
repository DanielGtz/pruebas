package test

class CatalogoController {

    static scaffold = true

    def cambiaComboEquipos(Long idCiudad) {
    	Ciudad ciudadInstance = Ciudad.get(idCiudad)
    	render(template: 'seleccionEquipo', model:  [equipos: ciudadInstance.equipos])
    }
}
