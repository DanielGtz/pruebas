package test

class Alumno {

	String nombreAlumno

	static belongsTo	=	[materia: Materia]

    static constraints = {
    	nombreAlumno()
    	materia()
    }

    String toString() {
    	return nombreAlumno
    }
}
