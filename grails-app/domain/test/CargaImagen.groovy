package test

class CargaImagen {

	String nombreImagen
	byte[] imagen
	String urlImagen

	static mapping = {
	}

    static constraints = {
    	nombreImagen()
    	imagen(maxSize: 1073741824)
    	urlImagen(nullable: true)
    }
}
