package test

class Materia {

	String nombreMateria

	static hasMany	=	[alumnos: Alumno]

    static constraints = {
    	nombreMateria() 
    	alumnos()
    }
    
    String toString() {
    	return nombreMateria
    }
}
