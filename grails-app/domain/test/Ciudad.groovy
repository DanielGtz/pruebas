package test

class Ciudad {
	
	String	nombreCiudad
	
	static hasMany	=	[equipos: Equipo]

    static constraints = {
    	nombreCiudad()
    	equipos()
    }

    String toString() {
    	return nombreCiudad
    }
}