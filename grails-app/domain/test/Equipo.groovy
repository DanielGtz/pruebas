package test

class Equipo {

	String nombreEquipo

	static belongsTo	=	[ciudad: Ciudad]
	
    static constraints = {
    	nombreEquipo()
    	ciudad()
    }

    String toString() {
    	return nombreEquipo
    }
}