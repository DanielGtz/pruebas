<%@ page import="test.Catalogo" %>

<g:javascript library="jquery" />

<div class="fieldcontain ${hasErrors(bean: catalogoInstance, field: 'nombre', 'error')} ">
	<label for="nombre">
		<g:message code="catalogo.nombre.label" default="Nombre" />
		
	</label>
	<g:textField name="nombre" value="${catalogoInstance?.nombre}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: catalogoInstance, field: 'ciudad', 'error')} required">
	<label for="ciudad">
		<g:message code="catalogo.ciudad.label" default="Ciudad" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="ciudad" name="ciudad.id" from="${test.Ciudad.list()}" optionKey="id" required="" value="${catalogoInstance?.ciudad?.id}" class="many-to-one" />
</div>


<div class="fieldcontain ${hasErrors(bean: catalogoInstance, field: 'equipo', 'error')} required">
	<label for="equipo">
		<g:message code="catalogo.equipo.label" default="Equipo" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="equipo" name="equipo.id" from="" optionKey="id" required="" value="${catalogoInstance?.equipo?.id}" class="many-to-one"/>
</div>


<r:script>
	$("#ciudad").change(function() {
		$("#equipo").load("${createLink(action: 'cambiaComboEquipos')}?idCiudad="+this.value);
	});
</r:script>

