
<%@ page import="test.Catalogo" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'catalogo.label', default: 'Catalogo')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-catalogo" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-catalogo" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="nombre" title="${message(code: 'catalogo.nombre.label', default: 'Nombre')}" />
					
						<th><g:message code="catalogo.ciudad.label" default="Ciudad" /></th>
					
						<th><g:message code="catalogo.equipo.label" default="Equipo" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${catalogoInstanceList}" status="i" var="catalogoInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${catalogoInstance.id}">${fieldValue(bean: catalogoInstance, field: "nombre")}</g:link></td>
					
						<td>${fieldValue(bean: catalogoInstance, field: "ciudad")}</td>
					
						<td>${fieldValue(bean: catalogoInstance, field: "equipo")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${catalogoInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
