
<%@ page import="test.CargaImagen" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'cargaImagen.label', default: 'CargaImagen')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-cargaImagen" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-cargaImagen" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="nombreImagen" title="${message(code: 'cargaImagen.nombreImagen.label', default: 'Nombre Imagen')}" />
					
						<g:sortableColumn property="imagen" title="${message(code: 'cargaImagen.imagen.label', default: 'Imagen')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${cargaImagenInstanceList}" status="i" var="cargaImagenInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${cargaImagenInstance.id}">${fieldValue(bean: cargaImagenInstance, field: "nombreImagen")}</g:link></td>
					
						<td>${fieldValue(bean: cargaImagenInstance, field: "imagen")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${cargaImagenInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
