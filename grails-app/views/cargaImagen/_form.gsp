<%@ page import="test.CargaImagen" %>



<div class="fieldcontain ${hasErrors(bean: cargaImagenInstance, field: 'nombreImagen', 'error')} ">
	<label for="nombreImagen">
		<g:message code="cargaImagen.nombreImagen.label" default="Nombre Imagen" />
		
	</label>
	<g:textField name="nombreImagen" value="${cargaImagenInstance?.nombreImagen}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: cargaImagenInstance, field: 'imagen', 'error')} required">
	<label for="imagen">
		<g:message code="cargaImagen.imagen.label" default="Imagen" />
		<span class="required-indicator">*</span>
	</label>
	<input type="file" id="imagen" name="imagen" />
</div>

