
<%@ page import="test.CargaImagen" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'cargaImagen.label', default: 'CargaImagen')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-cargaImagen" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-cargaImagen" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list cargaImagen">
			
				<g:if test="${cargaImagenInstance?.nombreImagen}">
				<li class="fieldcontain">
					<span id="nombreImagen-label" class="property-label"><g:message code="cargaImagen.nombreImagen.label" default="Nombre Imagen" /></span>
					
						<span class="property-value" aria-labelledby="nombreImagen-label"><g:fieldValue bean="${cargaImagenInstance}" field="nombreImagen"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${cargaImagenInstance?.imagen}">
				<li class="fieldcontain">
					<span id="imagen-label" class="property-label"><g:message code="cargaImagen.imagen.label" default="Imagen" /></span>

					<span class="property-value" aria-labelledby="nombreImagen-label">
						<img src="
							<g:createLink controller='cargaImagen' action='renderizarImagen' id='${cargaImagenInstance?.id}'/>
						">
					</span>
					
				</li>
				</g:if>

				<g:if test="${cargaImagenInstance?.urlImagen}">
				<li class="fieldcontain">
					<span id="urlImagen-label" class="property-label"><g:message code="cargaImagen.urlImagen.label" default="URL Imagen" /></span>
					
					<span class="property-value" aria-labelledby="urlImagen-label"><g:fieldValue bean="${cargaImagenInstance}" field="urlImagen"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${cargaImagenInstance?.id}" />
					<g:link class="edit" action="edit" id="${cargaImagenInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
