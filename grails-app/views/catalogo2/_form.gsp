<%@ page import="test.Catalogo2" %>
	
<g:javascript library="jquery" />


<div class="fieldcontain ${hasErrors(bean: catalogo2Instance, field: 'nombre', 'error')} ">
	<label for="nombre">
		<g:message code="catalogo2.nombre.label" default="Nombre" />
		
	</label>
	<g:textField name="nombre" value="${catalogo2Instance?.nombre}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: catalogo2Instance, field: 'materia', 'error')} required">
	<label for="materia">
		<g:message code="catalogo2.materia.label" default="Materia" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="materia" name="materia.id" from="${test.Materia.list()}" optionKey="id" required="" value="${catalogo2Instance?.materia?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: catalogo2Instance, field: 'alumno', 'error')} required">
	<label for="alumno">
		<g:message code="catalogo2.alumno.label" default="Alumno" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="alumno" name="alumno.id" from="" optionKey="id" required="" value="${catalogo2Instance?.alumno?.id}" class="many-to-one"/>
</div>

<r:script>
	$("#materia").change(function() {
		$("#alumno").load("${createLink(action: 'cambiaComboAlumno')}?idMateria="+this.value);
	});
</r:script>

