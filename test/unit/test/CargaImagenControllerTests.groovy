package test



import org.junit.*
import grails.test.mixin.*

@TestFor(CargaImagenController)
@Mock(CargaImagen)
class CargaImagenControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/cargaImagen/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.cargaImagenInstanceList.size() == 0
        assert model.cargaImagenInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.cargaImagenInstance != null
    }

    void testSave() {
        controller.save()

        assert model.cargaImagenInstance != null
        assert view == '/cargaImagen/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/cargaImagen/show/1'
        assert controller.flash.message != null
        assert CargaImagen.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/cargaImagen/list'

        populateValidParams(params)
        def cargaImagen = new CargaImagen(params)

        assert cargaImagen.save() != null

        params.id = cargaImagen.id

        def model = controller.show()

        assert model.cargaImagenInstance == cargaImagen
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/cargaImagen/list'

        populateValidParams(params)
        def cargaImagen = new CargaImagen(params)

        assert cargaImagen.save() != null

        params.id = cargaImagen.id

        def model = controller.edit()

        assert model.cargaImagenInstance == cargaImagen
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/cargaImagen/list'

        response.reset()

        populateValidParams(params)
        def cargaImagen = new CargaImagen(params)

        assert cargaImagen.save() != null

        // test invalid parameters in update
        params.id = cargaImagen.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/cargaImagen/edit"
        assert model.cargaImagenInstance != null

        cargaImagen.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/cargaImagen/show/$cargaImagen.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        cargaImagen.clearErrors()

        populateValidParams(params)
        params.id = cargaImagen.id
        params.version = -1
        controller.update()

        assert view == "/cargaImagen/edit"
        assert model.cargaImagenInstance != null
        assert model.cargaImagenInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/cargaImagen/list'

        response.reset()

        populateValidParams(params)
        def cargaImagen = new CargaImagen(params)

        assert cargaImagen.save() != null
        assert CargaImagen.count() == 1

        params.id = cargaImagen.id

        controller.delete()

        assert CargaImagen.count() == 0
        assert CargaImagen.get(cargaImagen.id) == null
        assert response.redirectedUrl == '/cargaImagen/list'
    }
}
